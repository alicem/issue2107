
int main (string[] args) {
	var app = new Gtk.Application ("org.example.issue2107", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new Issue2107.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
